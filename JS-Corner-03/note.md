file:///F:/Development/barcelona250y/svn/fcn/CanvasGrid（仮）/source/branches/develop/CanvasGrid_0.2/index.html

---

# Canvas

主流两大应用：
- 游戏
- 图表

---

路径操作

线：moveTo/lineTo  
beginPath
closePath
stroke
fill

矩形

rect  
strokeRect  
fillRect

---

样式

strokeStyle  
lineWidth

---

requestAnimationFrame  
webkitrequestAnimationFrame

16ms -- 1000/16 约62帧每秒

---

canvas动画：canvas不保存图形对象，每次擦了重画
1.全擦
2.动画：requestAnimationFrame

事件：没有图形对象，只有canvas自身有事件。自己动手：检测位置（碰撞测试）

---

饼图
- 角度与弧度转换
- 计算弧上点的位置

---

文字：

strokeText  
fillText

---

## transform
canvas 左上角基点  
1.rotate  
2.translate  
3.scale

(no： skew)

必用：
1. beginPath
2. save/restore

--- 

## 图片操作（90%--大部分应用在于）

```js
drawImage(
    oImg,
    sx, sy, sw, sh,
    dx, dy, dw, dh
)
```

## 像素级操作

一个像素占4位：rgba

  r 0~255  
  g 0~255  
  b 0~255  
  a 0~1  

## 图形后续处理
