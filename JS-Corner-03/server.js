const http = require('http')
const fs = require('fs')

const port = process.env.port || 6600
const server = http.createServer(function(req, res){

    const url = req.url
    const hp = './index.html'
    let page = hp
    let html = ''

    if(url !== '/'){
        page = `.${url}`
    }

    try{
        html = fs.readFileSync(page)
    }
    catch(e){
        html = fs.readFileSync(hp)
    }
    res.end(html)
})

server.listen(port)
console.log(`Visit: http://localhost:${port} to access.`)