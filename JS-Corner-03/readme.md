# 浅谈 Canvas 的应用

## 案例 demo

按照以下步骤查看本示例：

（如果已经clone过的，直接进入JS-Corner-03目录即可）v

```
git clone https://gitee.com/coder-monkey/JS-Corner.git

cd JS-Corner-03

npm start
```

如果端口号（demo中6600）在你的环境中已占用，随便改下别的号再启动就好了。

canvas 主要应用在于做图表和游戏。

这里的示例有：

1. 做一个饼图  
  画线和画弧

2. 做滤镜，包括给图片和视频  
  示例：灰化，泛黄，黑白三个滤镜。

3. 游戏方面，做一个人物走动的动效  
  资料：用一张行走图（或叫雪碧图）

1和3不需要服务器直接查看html文件就可以，  
2的话需要启动服务器（涉及跨域问题）

运行起来，访问页面，就可以看到效果了。
```
http://localhost:6600
```

---

## canvas 主要被应用于

- 图表（比如：Echarts）

- 游戏（比如：微信小游戏）

- 其它的话，本人不太了解

  但是像大家熟知的Photoshop，  
  里面有各种滤镜，那canvas也是都能做出来的。  
  不了解有哪些 **明星产品** ，但不可否认canvas什么都能画，  
  因为可以操作像素级的数据。  
  （一般的话都能应付，除非太大型了会有性能问题）

---

## 补充：
基于当前示例代码（2019-09-18）继续完善，  
比如饼图加文字，比如自己实现个其它滤镜，  
或者完成一个小游戏，根据自己爱好，动手实践吧!
