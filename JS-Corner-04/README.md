# JS-Corner-04

小伙伴们，时隔一个月，第四期来了。继续上次的话题，还是canvas。  
不过，我们让画东西变得简单了，使用`canvas-components`。

## 什么是`canvas-components`?

> 现代前端框架赋予了开发们简便的组件化开发方式，实现业务抽离与代码复用，提升开发效率代码整洁方便维护。

近日看了点`react`，于是，用`react`试着实现`canvas`的常用基础图形以及显示图片的功能（暂时做到这里了）。

然后，把它发布到了`npm`上，虽然还简陋，算不上轮子，也算有个铁环了吧（就是我们这代人小时候在街上玩的玩具了）。

## 回顾下上次如何画图形的

基本是这样婶儿的，要写一堆 js，指挥笔头去这儿去那儿的：
```
function pie(startAng, endAng, color){
    let x = cx+Math.cos(d2a(startAng))*r
    let y = cy+Math.sin(d2a(startAng))*r
    gd.beginPath()
    gd.moveTo(cx, cy)
    gd.lineTo(x, y)
    gd.arc(cx,cy,r,d2a(startAng), d2a(endAng), false)
    gd.closePath()
    gd.fillStyle = color
    gd.fill()
}
```

## 这次就简单些了

只需要对应的 Tag 标签，里面指定所需的数值或样式等，So easy ：
```
// Demo data for Pie
let cx = 150, cy=120, r=100
let circleCenter = {x:cx, y:cy}
let data = [
{ name: 'React', share: 42, color: '#C2B', angle:{start:180,end:330} },
{ name: 'Vue', share: 33, color: '#FA0', angle:{start:330,end:450} },
{ name: 'Angular', share: 25, color: '#BFF', angle:{start:450,end:540} },
]

<Canvas width={500} height={300}>
    <Line from={{x: 190,y:20}} to={{x:290, y:130}} stroke='darkblue' shadow={{color:'#FF00FF', blur:2, offsetX:10,offsetY:0}}></Line>
    <Rect x='20' y='20' width='150' height='100' stroke='blue' fill='yellow' shadow={{color:'#00eeee', blur: 5, offsetX: 10, offsetY: 10}}></Rect>
    {
        data.map((item,index)=>{
        return (
            <Pie key={index+item} angle={item.angle} radius={r} center={circleCenter} stroke='#FF0000' fill={item.color}></Pie>
        )
        })
    }
</Canvas>
```
图形就是堆标签（Canvas，Line，Rect，Pie），  
图片也是标签：
```
<Canvas width={400} height={300}>
    <Img src='/examples/src/clover.png' stretch='fill' filter='gray'></Img>
</Canvas>
```
还可以设平铺还是伸缩填充以及设滤镜（默认的或自定义的）。

这就是目前的功能了。更多细节，请参考
- [Github: canvas-components](https://github.com/CoderMonkie/canvas-components/tree/master/examples)
- [Gitee: canvas-components](https://gitee.com/coder-monkey/canvas-components)

> [如何同时关联多个网上仓库，实现一次`push`多站提交（github + gitee）](https://www.cnblogs.com/CoderMonkie/p/git-multi-origin.html)

---

## 如何使用

包已经发布到`npm`，所以，很简单，只要安装一下，就可以在你的react项目中使用了：
```
npm install canvas-compopnents
```
> 建议使用`cnpm`，不过注意工程里的包要统一安装源，否则可能会启动不了

## 关于这个组件：  

其实就是将js操作canvas的api处理，封装起来，屏蔽掉了基础操作，让`UI`用起来更简洁。  
发布到`npm`着实折腾了一阵子。感兴趣的童鞋，赶紧装上试试吧，一起改善它，让轮子转起来！

关于如何发布`npm`包，之后再整理一篇吧。

~走过路过，start一过啊亲~

> [canvas-components](https://github.com/CoderMonkie/canvas-components)  

> 代码看不懂的童鞋，要学习下 react 喽

---

## example :
![demo](./example.jpg)