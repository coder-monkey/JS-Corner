![image.png](https://p6-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/79554febcab049c8a47e52961e996f56~tplv-k3u1fbpfcp-watermark.image)

vue2.x 的项目里，通过 new 一个 Vue 实例的方式，让它来充当事件总线，管理非父子组件之间的事件派发响应。

1. 先创建一个`eventHub.js`文件，编辑如下

```javascript
import Vue from "vue";
const eventHub = new Vue();
export default eventHub;
```

2. 监听事件的 vue 组件

```javascript
import eventHub from "@/eventHub.js";

export default {
  name: "监听事件的组件",
  created() {
    eventHub.$on("event-name", this.handler);
  },
  methods: {
    handler(params) {
      console.log("event-name handler", params);
    },
  },
};
```

3. 派发事件的 vue 组件

```javascript
import eventHub from "@/eventHub.js";

export default {
  name: "派发事件的组件",
  Mounted() {
    eventHub.$emit("event-name", "some params");
  },
};
```

---

![vue3.0](https://p1-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/2bf5fd51c4b5423dac1ccdfd9b9df8bd~tplv-k3u1fbpfcp-watermark.image)

Vue3.0 中取消 emit 的变动，使得没法再按上面的方式使用了。官方建议使用第三方库`mitt`。

1. 安装

```javascript
npm install --save mitt
```

2. 注册使用

创建一个`eventHub.js`文件

```javascript
import Mitt from "mitt";

const eventHub = new Mitt();

eventHub.$on = eventHub.on;
eventHub.$off = eventHub.off;
eventHub.$emit = eventHub.emit;

export default eventHub;
```

3. 将事件总线注册到 Vue 实例
   在`main.js`中

```javascript
import { createApp } from "vue";
import App from "./App.vue";
import eventHub from "@/plugins/eventHub.js";

const app = createApp(App);

// 配置全局事件总线
app.config.globalProperties.eventHub = eventHub;

app.mount("#app");
```

3. 监听事件的组件

```javascript
import { getCurrentInstance, onMounted } from "vue";

export default {
  name: "YourComponent",
  setup() {
    const eventHandler = async (params) => {
      // some async process
    };

    const { eventHub } = getCurrentInstance().proxy;
    eventHub.$on("event-name", eventHandler);

    onBeforeUnmount(() => {
      eventHub.$off("event-name", eventHandler);
    });
  },
};
```

4. 派发事件的组件

```javascript
import { getCurrentInstance, onMounted } from "vue";

export default {
  name: "",
  setup() {
    const { eventHub } = getCurrentInstance().proxy;

    onMounted(() => {
      eventHub.$emit("event-name", "some params");
    });
  },
};
```

注：

- 上面封装的时候，使用`$emit / $on / $off`，是为了方便 Vue2.x 的项目做迁移
- 注意监听要在派发之前，Vue3.0 中生命周期的调用顺序要清楚

---

动动小手，简单实现一个上面的`mitt`工具类。

```javascript
class EventHub {
  constructor() {
    this.pool = {};
  }

  $on(event, handler) {
    this.pool[event] = this.pool[event] || new Set();

    this.pool[event].add(handler);
  }

  $off(event, handler) {
    this.pool[event].delete(handler);
  }

  $emit(event, params) {
    const handlers = this.pool[event];
    if (!handlers || !handlers.size) return;
    handlers.forEach((fn) => {
      fn(params);
    });
  }

  $clear(event) {
    this.pool[event] = undefined;
  }

  $clearAll() {
    this.pool = {};
  }
}

const eventHub = new EventHub();

export default eventHub;
```

使用方式与上方一致，代码无需改动，效果无异。

留作业环节：如果再要实现一个`$once`呢？

---

<div style="display:flex;justify-content:center">
  
![码路工人公众号](https://p3-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/e2cfd476e36c4130a917d166488e5212~tplv-k3u1fbpfcp-zoom-1.image)

</div>

</section>
