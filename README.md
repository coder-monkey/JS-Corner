# JS-Corner

## 介绍

JS-Corner. For JS Learning~

不定期分享前端（JS）相关有趣的内容，基础知识等。

喜欢或被迫喜欢 JS 的你不要错过呀~

一起学习共同精进！

## Content

|No|Article Title||
|---|---|---|
|01|[带你领略正则的美妙](./JS-Corner-01/README.md)||
|02|[关于“字符串逃逸”](./JS-Corner-02/README.md)||
|03|[浅谈 Canvas 的应用](./JS-Corner-03/README.md)||
|04|[封装一个Canvas的库用在React项目中](./JS-Corner-04/README.md)||
|05|[使用 VuePress 搭建个人博客](./JS-Corner-05/README.md)|[Read](https://mp.weixin.qq.com/s?__biz=Mzg5MzIzMTA0MQ==&mid=2247483813&idx=1&sn=469036f758eb17bd56448bd91e2e659d&chksm=c0334b0bf744c21d8112044f835f17ed7d955b3287a6a2945546fd7402681ba1bb203cf4b640&token=727181561&lang=zh_CN#rd)|
|06|[将VuePress建立的博客部署到GitHub或Gitee上](./JS-Corner-06/README.md)|[Read](https://mp.weixin.qq.com/s?__biz=Mzg5MzIzMTA0MQ==&mid=2247483818&idx=1&sn=7c3bea4260221a722d52ae300b23b6f3&chksm=c0334b04f744c212ce20137b360e37234605664bc5dcfd2e17cd975e2655e2b5e8cc14e90411&token=727181561&lang=zh_CN#rd)|
|07|[如何同时关联多个远程仓库](./JS-Corner-07/README.md)|[Read](http://mp.weixin.qq.com/s?__biz=Mzg5MzIzMTA0MQ==&mid=2247483758&idx=1&sn=f3d1e9985fcc761ac653819a86b59164&chksm=c0334bc0f744c2d682ddd0e5289659a823a51b68dfb2004b20b0e1b2778845d9a8d598956031&token=727181561&lang=zh_CN#rd)|
|08|[npm/nrm/nvm全家桶让你的npm更锋利](./JS-Corner-08/README.md)|[Read](https://mp.weixin.qq.com/s?__biz=Mzg5MzIzMTA0MQ==&mid=2247483753&idx=1&sn=2c3aacf8d76ab6abfc55ed67b0cd6b07&chksm=c0334bc7f744c2d1f69b2ba8f6db39448da29f39127ae6d9d9fcdc30863a7b7fc71c417ad100&token=727181561&lang=zh_CN#rd)|
|09|[[jQuery插件]手写一个图片懒加载实现](./JS-Corner-09/README.md) |[Read](https://mp.weixin.qq.com/s?__biz=Mzg5MzIzMTA0MQ==&mid=2247483947&idx=1&sn=8b7e0e3c7b5c69b93bc189a0d5dc340c&chksm=c0334885f744c19398c9dec76b0c8f780ba97afbc65cf51975f8aef0abbcd36eef4fd264c476&token=727181561&lang=zh_CN#rd)|
|10|[JS中的函数防抖(debounce)与节流(throttle)--不懵圈指北](./JS-Corner-10/README.md)|[Read](https://mp.weixin.qq.com/s?__biz=Mzg5MzIzMTA0MQ==&mid=2247483952&idx=1&sn=0124ef123790ef4b3d4df98af3931ab0&chksm=c033489ef744c1882a6482df04496170ac18b98cb0b6563ab86780d08f45d17367def2deec70&token=727181561&lang=zh_CN#rd)|
|11|[关于版本语义化及个人的一点浅薄认识和实践](./JS-Corner-11/README.md)|[Read](https://juejin.cn/post/6912471852132597768/)|
|12|[Vue3.0 项目中使用事件总线](./JS-Corner-12/README.md)| [Read](https://juejin.cn/post/6977295887668084749)|
|13|||

## 公众号

<div style="display:flex;justify-content:center">
  
![CoderPower_QRCode](CoderPower_QRCode.jpg)

</div>
