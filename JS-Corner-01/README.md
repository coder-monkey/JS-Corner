> 最初放在 [Issue#01](https://gitee.com/coder-monkey/JS-Corner/issues/I11NQY) 中了，现在统一放在 source code 中，更方便查阅

# 【JS-Corner】第一期：带你领略正则的妙用

正文

## 应用场景1：JS正则表达式做表单校验

### （1）需求举例

常用，掌握等级：五颗星

> 比如密码校验：要求必须是数字、小写、大写，至少有其中两种,6到12位

### （2）代码实现
> 解析：
> - 全是数字：false
> - 全是小写：false
> - 全是大写：false
> - 类型符合，但位数不足6：false
> - 类型符合，但位数超过12：false
> - 其它情况：true

```js

var pwdReg = /(?!^[0-9]+$)(?!^[a-z]+$)(?!^[A-Z]+$)(^[0-9a-zA-Z]{6,12}$)/
 
// 只有数字：false
console.log(pwdReg.test("123456"))
 
// 只有小写：false
console.log(pwdReg.test("aabbcc"))
 
// 只有大写：false
console.log(pwdReg.test("AABBCC"))
 
// 类型符合，但位数不足6：false
console.log(pwdReg.test("123ab"))
 
// 类型符合，但位数超过12：false
console.log(pwdReg.test("1234567890Ab3"))
 
// 小写 + 大写：true
console.log(pwdReg.test("aBcdef"))
 
// 大写 + 数字：true
console.log(pwdReg.test("A12345"))
 
// 小写 + 数字：true
console.log(pwdReg.test("a12345"))
 
// 大写 + 小写 + 数字：true
console.log(pwdReg.test("Aa1234"))
```

## 应用场景2：内容格式化

### （1）需求举例

比较常用，掌握等级：四颗星

> 比如实现金额格式化：每三位数字加逗号（暂不考虑小数，或小数限制仅两位）

### （2）代码实现

> 解析：
> 通常做法，从结尾处开始找3位的数字一组，如果前面有数字，则中间加逗号。
> 【注意：需要全局匹配】

```js
const num = 1234567890123
let regExp = /(\d{1,3})(?=(\d{3})+$)/g

num.toString().replace(regExp, '$1,')
```

> 上面既然提到了通常做法，自然还有不通常的骚气操作：
> 将非单词边界替换为逗号

```js
const originNum = 1234567890123
let regExp = /\B(?=((?:(\d{3}))+(?:(\b))))/g

originNum.toString().replace(regExp, ',')
```

## 应用场景3：模板引擎！（老鸟肯定不陌生，萌新的话也许没想到吧 ）

### （1）需求举例

很少会用，掌握等级：两颗星

> 不管曾经的经典模式MVC，还是现代流行的MVVM，都少不了的：模板引擎  

前端主流三大框架自不必说，都有自己的模板引擎，连一般的JS库，也有相关实现。  
我们在这里可以简单实现一下，看看模板引擎怎么玩。

### （2）代码实现
> 解析：
> 在我们的简单示例中，考虑有三个功能，
> - 直接插值（DONE）
> - 表达式代码语句（DONE）
> - 字符串逃逸（TODO）

2019/09/04 掘金上的一篇，曝出微信公众号平台的bug，
[链接](https://juejin.im/post/5d6f1e68f265da03d871dc08?utm_source=gold_browser_extension)
文中的转义，也即字符串逃逸（escape），主要是针对为了避免XSS攻击。

我们模板引擎中用它可以显示出特殊字符。
篇幅有限，在下面的示例中写了TODO，没有去实现，
如果有对此感兴趣的同学，请告诉我，我们单独拿出一篇来讲字符串逃逸。

废话不多说，具体请看代码：

> [demo-template.html](https://gitee.com/coder-monkey/JS-Corner/blob/master/JS-Corner-01/demo-template.html)

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>【JS-Corner】第一期：带你领略正则的美妙</title>
</head>
<style>
.is-youth{
    color: red;
}
</style>
<body>
    <div>
        <pre>
            &lt;%= xxx %>       直接插值        interpolate
            &lt;%- xxx %>       字符串逃逸      escape
            &lt;%  xxx %>       表达式代码语句  expression
        </pre>
    </div>

    <hr>

    <div id="container"></div>

    <script src="./template.js"></script>

    <script type="text/template" id="template">
        <ul class="list">
            <% obj.forEach(function(e){ %>
                <% if(e.age < 18){ %>
                    <li class="is-youth">
                        <%= e.name %> ---- <%= e.age %> ： ~young~
                    </li>
                <% } else { %>
                    <li>
                        <%= e.name %> ---- <%= e.age %>
                    </li>
                <% } %>
            <% }) %>
        </ul>
    </script>
    
    <script>
        // 数据
        var data = [
            {
                name: "CoderMonkey",
                age: 18
            },
            {
                name: "CoderMonkie",
                age: 9
            }
        ]

        // 自定义模板规则设置
        var settings = {
            interpolate: /<%:([\s\S]+)%>/,
            escape: /<%!([\s\S]+)%>/,
        }

        var templateString = document.getElementById('template').innerHTML

        var compiled = template(templateString)
        // 可自定义模板规则：
        // var compiled = template(templateString, settings)

        var result = compiled(data)

        document.querySelector('#container').innerHTML = result

    </script>
</body>
</html>
```

> [template.js](https://gitee.com/coder-monkey/JS-Corner/blob/master/JS-Corner-01/template.js)

```js
(function(root){

    const RULES = {
        interpolate: /<%=([\s\S]+?)%>/,
        escape: /<%-([\s\S]+?)%>/,
        expression: /<%([\s\S]+?)%>/,
    }

    function template(templateString, settings){
        // var matcher = /<%=([\s\S]+?)%>|<%-([\s\S]+?)%>|<%([\s\S]+?)%>/g
        // 将规则可自定义化，避免↑这样硬编码

        Object.assign(RULES, settings)

        var rule = new RegExp( [
            RULES.interpolate.source,
            RULES.escape.source,
            RULES.expression.source,
        ].join('|'), 'g')

        var source = "_p+='"
        var index = 0

        templateString.replace(rule, function(match, interpolate, escape, expression, offset){

            // 将非js部分内容拼接进去
            source += templateString.slice(index, offset).replace(/\n/g, function(){
                return "\\n"
            })
            index = offset + match.length       // #

            if(interpolate){
                source += "' + \n ((_t=(" + interpolate + ")) == null ? '' : _t) + \n'"
            }
            else if(escape){
                // TODO 字符串逃逸
            }
            else if(expression){
                source += "';\n"+ expression + "\n _p+='"
            }
        });
        source += " '; "

        source = "with(obj){\n" + source + "}\n"

        source = "var _t, _p='';\n" + source + " return _p;\n" 

        var render = new Function("obj", source)

        var compiledTemplate = function(data){
            return render.call(null, data)
        }
        return compiledTemplate
    }

    root.template = template
})(this)
```

