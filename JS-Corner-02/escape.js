(function(root){

    const escapeMap = {
        '<': '&lt;',
        '>': '&gt;',
        '"': '&quot;',
        "'": '&#x27;',
        '`': '&#x60;',
        '&': '&amp;',
    }

    function createEscaper(map){
        return function(txt, settings){

            Object.assign(map, settings)

            let regSource = `(?:${Object.keys(map).join('|')})`
            let regExp = new RegExp(regSource, "g")

            function replacer(match){
                return map[match]
            }

            return regExp.test(txt) ? txt.replace(regExp, replacer) : txt
        }
    }

    root.escape = createEscaper(escapeMap)
})(this)