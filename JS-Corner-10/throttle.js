/**
 * `throttle`简单的节流函数实现
 *
 * @param {function} cb 要执行的回调函数
 * @param {number} wait 要设置的节流时间间隔
 * @returns {function}
 */
const throttle = function (cb, wait) {
    // 参数检查
    //   cb：function
    //   wait：number
    if (!cb || toString.call(cb) !== '[object Function]') {
        throw new Error(`${cb} is not a function.`)
    }
    // 没有传 wait 参数时（包括等于0）
    if (!wait) {
        wait = 500 // 设置默认延时
    }
    // wait 参数须为正整数
    else if (!Number.isInteger(wait) || wait < 0) {
        throw new Error(`${wait} is invalid as optional parameter [wait]`)
    }
    
    // 用来记录上次执行的时刻
    let lasttime = Date.now()

    return function (...args) {
        const now = Date.now()
        // 两次执行的时间间隔
        const timespan = now - lasttime
        // 当间隔小于等待时间即处于冷却中
        const isCoolingDown = timespan < wait

        console.log(timespan, isCoolingDown ? 'is cooling down' : 'execute')

        // 如果还没冷却好，就等待
        if (isCoolingDown) return

        // 记录本次执行的时刻
        lasttime = Date.now()

        // 冷却好了
        cb.apply(null, args)
    }
}